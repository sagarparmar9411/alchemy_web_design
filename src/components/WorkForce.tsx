export const WorkForce = () => {
    return <section className="py-lg-5 py-md-4 py-3">
        <div className="container py-lg-5 py-md-4 py-3">
            <div className="row">
                <div className="col-md-6" data-aos="fade-right">
                    <img src="/assets/work_force.png" className="img-fluid" alt="work force"/>
                </div>
                <div className="col-md-6" data-aos="fade-left">
                    <p>For Employers</p>
                    <h2 className="my-2 mb-4 display-2 text-capitalize fw-bold">
                        <span className="text-capitalize color_green"> Workforce </span>
                        <span className="text-capitalize">At Your Fingertips </span>
                    </h2>
                    <div>
                        <ul>
                            <li className="pb-3" style={{listStyleImage: "url(/assets/right_tick.png)", marginRight: '10px'}}><span>Easy to use mobile & web platform</span></li>
                            <li className="pb-3" style={{listStyleImage: "url(/assets/right_tick.png)"}}><span><strong>45,000+</strong> workers</span></li>
                            <li className="pb-3" style={{listStyleImage: "url(/assets/right_tick.png)"}}><span>Realtime <strong>tracking</strong></span></li>
                            <li className="pb-3" style={{listStyleImage: "url(/assets/right_tick.png)"}}><span><strong>95%</strong> fulfillment rate</span></li>
                            <li className="pb-3" style={{listStyleImage: "url(/assets/right_tick.png)"}}><span>Preferred worker list</span></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </section>
}