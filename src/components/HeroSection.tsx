export const HeroSection = () => {
    return (
        <div className="row my-5 d-flex align-items-center">
            <div className="col-12 col-sm-5 col-md-5 col-lg-7 mb-4">
                <div className="text-left pb-4 pb-6" data-aos="fade-up">
                    <h2 className="my-2 mb-4 display-1 text-capitalize fw-bold">
                        <span className="text-capitalize">modern<br/>temp <span className="text-gradient">labour</span> </span>
                        <span className="theme-color-secondary text-capitalize color_green"> solutions</span>
                    </h2>
                </div>
                <div className="my-4 d-flex gap-2" data-aos="fade-up" data-aos-delay="100" >
                    <a className="custom_button grey px-3 py-2"
                       href="#find-workers">Find Workers</a>
                    <a className="custom_button green px-3 py-2"
                       href="#find-jobs">Find a Job</a>
                </div>
            </div>
            <div className="col-12 col-sm-7 col-md-7 col-lg-5 mb-4 gap-4 text-right position-relative banner_img d-flex"  data-aos="fade-right">
                <div>
                    <div className="pb-3">
                        <img alt="img" src="/assets/img_1.png" className="img-fluid img_1"/>
                    </div>
                    <div className="py-3 text-end d-none d-lg-block">
                        <img alt="img" src="/assets/img_2.png" className="img-fluid img_2"/>
                    </div>
                    <div className="pt-3">
                        <img alt="img" src="/assets/img_3.png" className="img-fluid img_3"/>
                    </div>
                </div>
                <div className="d-flex gap-4">
                    <div>
                        <div className="pb-4  d-none d-lg-block">
                            <img alt="img" src="/assets/img_4.png" className="img-fluid img_4"/>
                        </div>
                        <div className="pb-4 pt-lg-3 pt-5">
                            <img alt="img" src="/assets/img_5.png" className="img-fluid img_5"/>
                        </div>
                        <div className="pb-3">
                            <img alt="img" src="/assets/img_6.png" className="img-fluid img_6"/>
                        </div>
                    </div>
                    <div>
                        <div className="pb-md-5 pb-sm-4  d-none d-lg-block">
                            <img alt="img" src="/assets/img_7.png" className="img-fluid img_5"/>
                        </div>
                        <div className="pt-md-5 pt-sm-4  d-none d-lg-block">
                            <img alt="img" src="/assets/img_8.png" className="img-fluid img_6"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>)
}