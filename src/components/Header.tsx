import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

export const Header = () => {
    return <div>
        <Navbar expand="lg" className="navbar_setting">
            <Container>
                <Navbar.Brand href="#home"><img src="/assets/logo.svg" alt="logo"/></Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="m-auto gap-3">
                        <Nav.Link className="align-self-center" href="#find_workders">Find Workers</Nav.Link>
                        <Nav.Link className="align-self-center" href="#find_jobs">Find Jobs</Nav.Link>
                        <Nav.Link className="align-self-center" href="#about_us">About Us</Nav.Link>
                        <Nav.Link className="align-self-center" href="#resources">Resources</Nav.Link>
                        <Nav.Link className="align-self-center" href="#contact">Contact</Nav.Link>
                    </Nav>
                    <Nav className="m-auto gap-4">
                        <Nav.Link className="align-self-center fw-normal" href="#login">Login</Nav.Link>
                        <Nav.Link href="#open_account" className="header_login_button">Open an account</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    </div>
}