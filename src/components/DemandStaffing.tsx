export const DemandStaffing = () => {
    const backgroundImageUrl = '/assets/onDemandBG.svg';
    return <section id="on_demand_staffing" style={{
        backgroundImage: `url(${backgroundImageUrl})`,
        backgroundRepeat: 'no-repeat',
        backgroundColor: '#f0ffff',
        backgroundPosition: 'center'
    }} className="pt-lg-5 pt-md-4 pt-3" data-aos="fade-up">
        <div className="container">
            <div className="text-center pb-14 pb-12">
                <div className="">
                    <h2 className="my-2 d-flex text-capitalize justify-content-center fw-bolder">
                        <span><span className="color_green">How</span> <span>on-demand staffing works</span></span>
                    </h2>
                </div>
            </div>
            <div className="w-100 px-lg-5 py-lg-5 py-md-4 py-3">
                <div className="row">
                    <div className="col-6 col-md-6 col-lg-3 gap-6 ">
                        <div className="mb-4 relative">
                            <div className="staff-icon">
                                <img src="/assets/signup.png" alt="team"/>
                            </div>
                            <div>
                                <p className="mt-3 fs-5">Step 1</p>
                                <h3 className="mt-2 fw-bolder text-capitalize">Quick sign up</h3>
                            </div>
                        </div>
                    </div>
                    <div className="col-6 col-md-6 col-lg-3 gap-6 ">
                        <div className="mb-4 relative">
                            <div className="staff-icon">
                                <img src="/assets/post_jobs.png" alt="team"/>
                            </div>
                            <div>
                                <p className="mt-3 fs-5">Step 2</p>
                                <h3 className="mt-2 fw-bolder text-capitalize">Post Jobs 24/7</h3>
                                <p>Use our mobile app or web platform from the office or on the go, any time of day</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-6 col-md-6 col-lg-3 gap-6 ">
                        <div className="mb-4 relative">
                            <div className="staff-icon">
                                <img src="/assets/view_matches.png" alt="team"/>
                            </div>
                            <div>
                                <p className="mt-3 fs-5">Step 3</p>
                                <h3 className="mt-2 fw-bolder text-capitalize">View Matches</h3>
                                <p>With thousands of ready-to-go workers you can watch your jobs being filled in real
                                    time</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-6 col-md-6 col-lg-3 gap-6 ">
                        <div className="mb-4 relative">
                            <div className="staff-icon">
                                <img src="/assets/meet.png" alt="team"/>
                            </div>
                            <div>
                                <p className="mt-3 fs-5">Step 4</p>
                                <h3 className="mt-2 fw-bolder text-capitalize">We Do The Rest!</h3>
                                <p>We take care of payroll, deductions and insurance</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

}