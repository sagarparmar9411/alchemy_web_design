import {Header} from "./components/Header.tsx";
import 'bootstrap/dist/css/bootstrap.min.css';
import './_theme/css/style.scss'
import Container from "react-bootstrap/Container";
import {HeroSection} from "./components/HeroSection.tsx";
import React, {useEffect} from "react";
import {DemandStaffing} from "./components/DemandStaffing.tsx";
import {WorkForce} from "./components/WorkForce.tsx";
import AOS from 'aos';
import 'aos/dist/aos.css';

function App() {
    useEffect(() => {
        AOS.init({
            offset: 200,
            delay: 100,
            duration: 1000,
            easing: 'ease',
            once: true,
        });
    }, []);
    return (
        <React.Fragment>
            <Header/>
            <Container>
                <HeroSection/>
            </Container>
            <DemandStaffing/>
            <WorkForce/>
        </React.Fragment>
    )
}

export default App
